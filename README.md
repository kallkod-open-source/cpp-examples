# C++ Examples

## Build and run

```
git clone git@gitlab.com:kallkod-open-source/cpp-examples.git
mkdir build
cd build/
cmake ../cpp-examples/
make
./enum-class/enum-class
```