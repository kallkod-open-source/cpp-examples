#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <ctime>
#include <mutex>
#include <execution>
#include <numeric>

std::mutex m;

void run(int start, int end, unsigned long long& sum, std::reference_wrapper<std::vector<int>> vec)
{
    long long temp_sum = 0;
    for (int i = start; i < end; i++)
    {
        temp_sum += vec.get()[i];
    }
    std::lock_guard<std::mutex> guard(m);
    sum += temp_sum;
}

int main()
{
    int num = 2'000'000'000;
    std::vector<int> vec(num);
    unsigned long long sum = 0;
    for (int i = 0; i < num; i++)
    {
        vec[i] = i+1;
    }
   /* std::cout << "Sizeof vec: " << sizeof(vec[0]) * vec.size() << '\n';
    std::cout << "Sizeof int: " << sizeof(int) << '\n';*/
    std::cout << "add numbers end " << clock() / double(CLOCKS_PER_SEC) << '\n';
    /*std::thread t1(add1, std::ref(num), std::ref(sum), 'A');
    std::thread t2(add2, std::ref(num), std::ref(sum), 'B');
    std::thread t3(add3, std::ref(num), std::ref(sum), 'C');
    std::thread t4(add4, std::ref(num), std::ref(sum), 'D');*/
#if 0
    std::thread t1(run, 0, vec.size() / 4, std::ref(sum), std::ref(vec));
    std::thread t2(run, vec.size() / 4, vec.size() / 2, std::ref(sum), std::ref(vec));
    std::thread t3(run, vec.size() / 2, vec.size() * 0.75, std::ref(sum), std::ref(vec));
    std::thread t4(run, vec.size() * 0.75, vec.size(), std::ref(sum), std::ref(vec));
    t1.join();
    t2.join();
    t3.join();
    t4.join();

#else
   
    for (auto& e : vec)
    {
        sum += e;
    }
#endif

    //sum = std::reduce(std::execution::par_unseq, vec.begin(), vec.end());
    std::cout << sum << '\n';
    std::cout << "sum end " << clock() / double(CLOCKS_PER_SEC) << '\n';
    return 0;
}