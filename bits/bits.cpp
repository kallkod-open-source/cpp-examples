#include <iostream>
#include <vector>
#include <cstdint>

namespace {
void int2bits(int32_t value, std::vector<int32_t>& bits, int32_t mask)
{
    if (value)
    {
        bits.push_back(value & mask ? 1 : 0);
        value &= ~mask;
        int2bits(value, bits, mask * 2);
    }
}
}

void int2bits(int32_t value, std::vector<int32_t>& bits)
{
    int32_t mask = 1;
    if (value)
    {
        bits.push_back(value & mask ? 1 : 0);
        value &= ~mask;
        int2bits(value, bits, mask * 2);
    }
}

int main(int argc, const char** argv)
{
    for (int x = 1; x < argc; ++x)
    {
        int value = std::atoi(argv[x]);

        std::vector<int32_t> bits;
        bits.reserve(32);
        int2bits(value, bits);
        while (bits.size() < 32)
            bits.push_back(0);

        std::cout << value <<": ";
        for (auto i = bits.rbegin(); i != bits.rend(); ++i)
            std::cout << *i;
        std::cout << "\n";
    }
}
