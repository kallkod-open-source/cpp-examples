#include "fruit.h"
#include <iostream>

using namespace std;

int main()
{
    Fruit fruit = Fruit::apple;

    do 
    {
        try
        {
            cout << "Enter fruit name:";
            cin >> fruit;
        }
        catch(const std::exception& e)
        {
            cerr << e.what() << "\n";
            continue;
        }
        break;
    } while(true);
    std::cout << fruit << " is " << (fruit == Fruit::apple ? "red" : "green") << "\n";
}