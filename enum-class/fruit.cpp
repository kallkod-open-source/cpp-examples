#include "fruit.h"
#include <iostream>
#include <tuple>

namespace
{
    const std::pair<Fruit, const char*> conversion[] = {
        { Fruit::apple, "apple" },
        { Fruit::kiwi,  "kiwi"  },
    };
}

std::ostream& operator<<(std::ostream& os, Fruit fruit)
{
    for (const auto& c : conversion)
    {
        if (c.first == fruit)
        {
            os << c.second;
            return os;
        }
    }
    return os;
}

std::istream& operator>>(std::istream& is, Fruit& fruit)
{
    std::string s;
    is >> s;
    for (const auto& c : conversion)
    {
        if (c.second == s)
        {
            fruit = c.first;
            return is;
        }
    }
    throw std::runtime_error("Unexpected value:" + s);
}