﻿#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Employee
{
private:
	static int id_generator;
	int id;
	string name = "";

public:
	
	Employee()
	  : id(++id_generator)
	{
	}

	Employee(string name) : id(++id_generator), name(name) {}

	virtual ~Employee() = default;

	virtual int CalculateSalary() const
	{
		return 0;
	}

	virtual void AskSalary()
	{
		cout << "Please enter employee name: ";
		getline(cin, name);
	}

	void print(ostream& os) const 
	{
		os << "Payroll for: " << id << " - " << name << "\n"
		   << "- Check amount: " << CalculateSalary() << "\n\n";
	}
};

int Employee::id_generator = 0;

class SalaryEmployee : public Employee
{
	int monthly_salary = 0;

public:

	SalaryEmployee() = default;
	
	void AskSalary() override
	{
		Employee::AskSalary();
		cout << "Please enter monthly salary: ";
		cin >> monthly_salary;
		cin.ignore();
	}
	
	int CalculateSalary() const override
	{
		return monthly_salary;
	}
};

class HourlyEmployee : public Employee
{
	int hour_rate = 0, hours_worked = 0;

public:
	HourlyEmployee() = default;
	
	void AskSalary() override
	{
		Employee::AskSalary();
		cout << "Please enter hour rate: ";
		cin >> hour_rate;
		cout << "Please enter hours worked: ";
		cin >> hours_worked;
		cin.ignore();
	}
	
	int CalculateSalary() const override
	{
		return hour_rate * hours_worked;
	}
};

class CommissionEmployee : public SalaryEmployee
{
	int commision = 0;

public:
	
	void AskSalary() override
	{
		SalaryEmployee::AskSalary();
		cout << "Please enter commission: ";
		cin >> commision;
		cin.ignore();
	}
	
	int CalculateSalary() const override
	{
		return SalaryEmployee::CalculateSalary() + commision;
	}
};

class PayrollSystem
{
	vector<Employee*> employees;
public:

	PayrollSystem() = default;
	~PayrollSystem()
	{
		for (auto e: employees)
		{
			delete e;
		}
	}

	PayrollSystem(const PayrollSystem&) = delete;
	PayrollSystem& operator=(const PayrollSystem&) = delete;
	PayrollSystem(PayrollSystem&&) = delete;
	PayrollSystem& operator=(PayrollSystem&&) = delete;

	void menu()
	{
		int value = 9;
		while (value != 0)
		{
			cout << "Salary type\n";
			cout << "-----------\n";
			cout << "(1) Monthly\n";
			cout << "(2) Hourly\n";
			cout << "(3) Commission\n";
			cout << "(0) Quit\n";
			cout << "Please enter salary type: ";
			cin >> value;
			cin.ignore();
			Employee* employee = nullptr;
			switch (value)
			{
			case 1:
			{
				employee = new SalaryEmployee;
				break;
			}
			case 2:
			{
				employee = new HourlyEmployee;
				break;
			}
			case 3:
			{
				employee = new CommissionEmployee;
				break;
			}
			case 0:
			{
				cout << "\n\n";
				break;
			}
			default:
				cout << "Please write a correct number (0-3)\n";
			}
			if (employee)
			{
				employee->AskSalary();
				employees.push_back(employee);
				employee = nullptr;
			}
		}
	}

	void print(ostream& os)
	{
		for (auto e: employees)
		{
			e->print(os);
		}
	}
};

int main()
{
	PayrollSystem list;
	list.menu();
	list.print(cout);
	return 0;
}
