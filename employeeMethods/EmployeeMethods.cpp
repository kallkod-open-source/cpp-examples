#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;

class employee
{
public:
	int id;
	string name;
	employee(int id, string name) : id(id), name(name)
	{
	}
	virtual void print(std::ostream& os = std::cout) const
	{
		os << "id: " << id << " name: " << name << endl;
	}
	virtual int calculatesalary() const
	{
		return 0;
	}
};

class salaryemployee : public employee
{
public:
	int monthly_salary;
	salaryemployee(int id, string name, int monthly_salary) : employee(id, name), monthly_salary(monthly_salary)
	{
	}
	void print(std::ostream& os = std::cout) const override
	{
		os << "id: " << id << " name: " << name << endl;
	}
	virtual int calculatesalary() const override
	{
		return monthly_salary;
	}
};

vector<string> mysplit(string sentence, char sep)
{
	vector<string> vec;
	size_t start;
	size_t end = 0;

	while ((start = sentence.find_first_not_of(sep, end)) != string::npos)
	{
		end = sentence.find(sep, start);
		vec.push_back(sentence.substr(start, end - start));
	}
	return vec;
}

class employeelist
{
public:
	vector<employee*> employees;

	employeelist() = default;

	employeelist(const employeelist&) = delete; // copy constructor
	employeelist& operator=(const employeelist&) = delete; // assignment operator

	employeelist(employeelist&&) = delete; // move constructor
	employeelist& operator=(employeelist&&) = delete; // move operator

	~employeelist()
	{
		for (auto p: employees)
		{
			delete p;
		}
	}

	void menu()
	{
		int value = 9;
		int quit = 0;

		while (value != quit)
		{
			cout << "payroll menu\n";
			cout << "============\n";
			cout << "(1) add employees\n";
			cout << "(2) write employees\n";
			cout << "(3) read employees\n";
			cout << "(4) print employees\n";
			cout << "(0) quit\n";
			cout << "please select: ";
			cin >> value;
			cout << '\n';
			switch (value)
			{
			case 1:
				cin.ignore();
				insert();
				break;
			case 2:
				writetofile();
				break;
			case 3:
				readfromfile();
				break;
			case 4:
				print();
				break;
			case 0:
				break;
			default:
				cout << "incorrect selection\n";
				break;
			}
		}
	}

	// ������
	vector<string> mysplit(string sentence, char sep)
	{
		vector<string> vec;
		size_t start;
		size_t end = 0;

		while ((start = sentence.find_first_not_of(sep, end)) != string::npos)
		{
			end = sentence.find(sep, start);
			vec.push_back(sentence.substr(start, end - start));
		}
		return vec;
	}

/*
	What should it do? Why is it inside class?
	vector<employee*> myjoin(vector<employee*> lst, char sep, vector<employee*> str)
	{
		vector<employee*> str1 = str;
		for (int i = 0; i < lst.size(); i++)
		{
			str1.emplace_back(lst[i], sep);
		}
		return str1;
	}
*/
	void writetofile() const
	{
		vector<employee*> str; // why is it names str???
		ofstream file("salary_employee.csv");
		if (file.is_open())
		{
			//myjoin(employees, ',', str); 
			for (int i = 0; i < str.size(); i++)
			{
				file << str[i] << '\n';
			}
		}
		cout << employees.size() << " employee(s) added to file " << "salary_employee.csv" << endl;
		file.close();
	}

	void readfromfile()
	{
		ifstream file("salary_employee.csv");
		if (file.is_open())
		{
			if (!file.eof())
			{
				
			}
		}
		file.close();
		cout << employees.size() << " employee(s) read from file " << "salary_employee.csv" << endl;
	}
	// �����
	
	void insert()
	{
		string name = " ";
		string quit = "0";
		int id = 1, salary;

		while (name != quit)
		{
			cout << "please enter employee name (0 to quit): ";
			getline(cin, name);
			if (name != quit)
			{
				cout << "please enter salary: ";
				cin >> salary;
				employees.push_back(new salaryemployee(id, name, salary));
				id++;
				cin.ignore();
			}
		}
	}

	void print() const
	{
		for (int i = 0; i < employees.size(); i++)
		{
			employees[i]->print();
		}
	}
};

int main()
{
	employeelist mylist;
	mylist.menu();
	return 0;
}
