#include <iostream>
#include <vector>
#include <memory>

struct Struct
{
    Struct()
    {
        throw 0;
    }
    ~Struct()
    {
        std::cout << "Destructor\n";
    }

    static void* operator new(std::size_t count)
    {
        void* ptr = ::operator new(count);
        std::cout << "Allocate new oblect, " << count << " bytes at "
            << ptr << "\n";
        return ptr;
    }

    static void operator delete(void* ptr)
    {
        std::cout << "Release memory at " << ptr << "\n";
        return ::operator delete(ptr);
    }
};

int main()
{
    std::unique_ptr<Struct> pVar;
    try
    {
        pVar.reset(new Struct);
    }
    catch(...)
    {
        std::cout << "Exception!\n";
    }

    std::cout << pVar.get() << '\n';
    return 0;
}
